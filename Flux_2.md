## Hal-hal yang di persiapkan,
- flux, download dari https://github.com/fluxcd/flux2/releases
- kustomize, download dari https://github.com/kubernetes-sigs/kustomize/releases
- Personal access token gitlab
- Akses pull private registry pada namespace flux-system (Secret gcr-json-key)

Struktur repository,
```bash
.
├── charts
│   └── project-name
│       ├── helm-chart-1
│       └── helm-chart-2
├── flux-system
│   ├── gotk-components.yaml
│   ├── gotk-sync.yaml
│   ├── kustomization.yaml
│   └── privy-releases.yaml
└── releases
    ├── kustomization.yaml
    └── namespace-name
        ├── service-name-1.yaml
        └── service-name-2.yaml
```

Pastikan konteks kubernetes cluster yang ingin di pasang sudah dalam cluster yang tepat,

```bash
export GITLAB_TOKEN=xxx
flux bootstrap gitlab --hostname=gitlab.privy.id --token-auth --owner=infrastructure/release --repository=[production|staging|development] --branch=main --components-extra=image-reflector-controller,image-automation-controller --interval 30s --verbose --version 0.17.2
```
> --repository pilih salah satu misal, development

```bash
results:
► connecting to https://gitlab.privy.id
► cloning branch "main" from Git repository "https://gitlab.privy.id/infrastructure/release/development.git"
✔ cloned repository
► generating component manifests
✔ generated component manifests
✔ committed sync manifests to "main" ("xxx")
► pushing component manifests to "https://gitlab.privy.id/infrastructure/release/development.git"
✔ installed components
✔ reconciled components
► determining if source secret "flux-system/flux-system" exists
► generating source secret
► applying source secret "flux-system/flux-system"
✔ reconciled source secret
► generating sync manifests
✔ generated sync manifests
✔ committed sync manifests to "main" ("xxx")
► pushing sync manifests to "https://gitlab.privy.id/infrastructure/release/development.git"
► applying sync manifests
✔ reconciled sync configuration
◎ waiting for Kustomization "flux-system/flux-system" to be reconciled
✔ Kustomization reconciled successfully
► confirming components are healthy
✔ helm-controller: deployment ready
✔ image-automation-controller: deployment ready
✔ image-reflector-controller: deployment ready
✔ kustomize-controller: deployment ready
✔ notification-controller: deployment ready
✔ source-controller: deployment ready
✔ all components are healthy
```
Semua komponen akan terinstall di namespace flux-system dan repository folder flux-system

Edit terlebih dulu `gotk-sync.yaml`
```yaml
---
apiVersion: source.toolkit.fluxcd.io/v1beta1
kind: GitRepository
metadata:
  name: flux-system
  namespace: flux-system
spec:
  interval: 30s
  ref:
    branch: main
  secretRef:
    name: flux-system
  url: https://gitlab.privy.id/infrastructure/release/development.git
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta1
kind: Kustomization
metadata:
  name: flux-system
  namespace: flux-system
spec:
  interval: 10m0s
  path: ./flux-system
  prune: true
  sourceRef:
    kind: GitRepository
    name: flux-system
  validation: client
```
Pada bagian Kustomization, di spesifik ke `spec.path: ./flux-system` untuk spesifik scan yaml flux-system

Update repo dan reconcile untuk sync,
```bash
git add & git commit & git push
flux reconcile kustomization flux-system --with-source
```

Tambahkan resource `flux-system/privy-release.yaml`
```yaml
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta1
kind: Kustomization
metadata:
  name: privy-releases
  namespace: flux-system
spec:
  interval: 30s
  path: ./releases
  prune: true
  sourceRef:
    kind: GitRepository
    name: flux-system
  validation: client
---
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImageUpdateAutomation
metadata:
  name: privysign-update-automation
  namespace: flux-system
spec:
  interval: 30s
  sourceRef:
    kind: GitRepository
    name: flux-system
  git:
    checkout:
      ref:
        branch: main
    commit:
      author:
        email: git-robot@privyid.tech
        name: git-robot
      messageTemplate: |
        Automated image update

        Automation name: {{ .AutomationObject }}

        Files:
        {{ range $filename, $_ := .Updated.Files -}}
        - {{ $filename }}
        {{ end -}}

        Objects:
        {{ range $resource, $_ := .Updated.Objects -}}
        - {{ $resource.Kind }} {{ $resource.Name }}
        {{ end -}}

        Images:
        {{ range .Updated.Images -}}
        - {{.}}
        {{ end -}}
    push:
      branch: main
  update:
    path: ./releases
    strategy: Setters
```

Update juga `kustomization.yaml` nya
```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- gotk-components.yaml
- gotk-sync.yaml
- privy-releases.yaml
```

Update repository, lalu reconcile untuk sync
```bash
git add & git commit & git push
flux reconcile kustomization flux-system --with-source
```

Opsional, jika ingin masukkan ke repo bagian `template/releases/releases-tmpl-semver.yaml`
```yaml
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImageRepository
metadata:
  name: PROJECT_NAME-REPO_NAME
  namespace: flux-system
spec:
  image: asia.gcr.io/privysign/PROJECT_NAME/REPO_NAME
  interval: 30s
  secretRef:
    name: gcr-json-key
---
apiVersion: image.toolkit.fluxcd.io/v1beta1
kind: ImagePolicy
metadata:
  name: PROJECT_NAME-REPO_NAME
  namespace: flux-system
spec:
  imageRepositoryRef:
    name: PROJECT_NAME-REPO_NAME
  policy:
    semver:
      range: '>=0.0.1'
---
apiVersion: helm.toolkit.fluxcd.io/v2beta1
kind: HelmRelease
metadata:
  name: REPO_NAME
  namespace: NAME_SPACE
spec:
  chart:
    spec:
      chart: ./charts/PROJECT_NAME/REPO_NAME
      sourceRef:
        kind: GitRepository
        name: flux-system
        namespace: flux-system
      interval: 1m0s
  interval: 30s
  values:
    image:
      repository: "asia.gcr.io/privysign/PROJECT_NAME/REPO_NAME" # {"$imagepolicy": "flux-system:PROJECT_NAME-REPO_NAME:name"}
      tag: "IMAGE_TAG" # {"$imagepolicy": "flux-system:PROJECT_NAME-REPO_NAME:tag"}
    replicaCount: 1
```

## Menambahkan chart dan release
Dan jika ingin menambahkan chart, caranya tambahkan helm chart ke direktori sesuai penamaan repo `charts/project-name/helm-chart-1` misal `charts/privysign/api-manage`

Lalu eksekusi ini untuk menambahkan release, sesuaikan `REPO_NAME`, `IMAGE_TAG`, `PROJECT_NAME`, dan `NAMESPACE`
```bash
REPO_NAME=api-manage IMAGE_TAG=xxx PROJECT_NAME=yyy NAMESPACE=zzz ; sed "s/PROJECT_NAME/${PROJECT_NAME}/g;s/REPO_NAME/${REPO_NAME}/g;s/IMAGE_TAG/${IMAGE_TAG}/g;s/NAME_SPACE/${NAMESPACE}/g" template/releases/releases-tmpl-semver.yaml > releases/${NAMESPACE}/${REPO_NAME}.yaml
```
Lalu restructure `kustomization.yaml` difolder `releases/`, agar releases yang sudah dibuat masuk kedalam referensi yang akan diapply kustomization
```bash
rm -f releases/kustomization.yaml ; cd releases/ ; kustomize create --autodetect --recursive ; cd ..
```
Preview kustomization,
```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- development-carstensz/bucket.yaml
- development-carstensz/cabinet.yaml
- development-carstensz/enterprise.yaml
- development-carstensz/grpcox.yaml
- development-carstensz/user.yaml
- development-privylite/basic-feature-document-service.yaml
- development-privylite/basic-feature-user-service.yaml
- development-privylite/helper-manage.yaml
```
Update repository
```bash
git add & git commit & git push
```
Lalu tunggu flux synchornization, atau kita bisa buat sync langsung dengan command ini,
```bash
flux reconcile kustomization privy-releases --with-source
```

## Summarization
Pada intinya flux 2 ini menerapkan konfigurasi yang lebih declarative dibanding flux 1, Core yang bertugas disini ada 3
- helm-controller  
Bertugas jadi helm controller, dengan kind `Helmrelease` dan crds `helmreleases.helm.toolkit.fluxcd.io`. Nah kind `Helmrelease` ini sudah ada di helm 1, terus apakah bentrok? Tentu saja tidak, pastikan komponen flux 1 (flux dan helm-operator) sudah ter down, bisa di scale ke 0 replicas.  Dibilang nggak bentrok karena api yang digunakan sudah beda, flux 1 menggunakan crds `helmreleases.helm.fluxcd.io`, flux 2 tinggal pickup kubernetes resource yang di stempel api `helm.toolkit.fluxcd.io`
- kustomize-controller  
Ini termasuk ke salah satu yang disebut lebih declarative, karena tiap-tiap yaml yang sudah dibuat di repo harus dimasukkan kedalam kustomize ini, jadi flux gak akan mentah-mentah pickup yaml resource yang ada di repo lalu memasang nya di kubernetes, melainkan menggunakan referensi kustomize ini, dan ya bahkan kita bisa memasukkan kustomization resource di dalam list kustomization ini
- source-controller  
Bertugas sebagai sumber nya si flux, jadi diantara kustomization atau helmchart sumbernya akan bersumber darisini, dan salah satu sumbernya Git repository. Masuk di `flux-system/gotk-sync.yaml`

Flux 1 khusus untuk metode glob (*) scan, cara kerjanya ia akan fetch metadata tiap-tiap image yang masuk, di extract dan diambil khusus timestamp nya. Timestamp tersebut di sortir berdasarkan waktu terbaru. Kelemahan nya di fetch metadata itu, referensi :
- https://github.com/fluxcd/flux2/discussions/802#discussion-2163488
- https://fluxcd.io/legacy/flux/faq/#flux-v1-runtime-behavior-doesnt-scale-well

Di flux 2, udah nggak menggunakan glob * lagi, misal `develop-*` melainkan diganti dengan regex, nanti kita tetap tagging dengan develop-commit-EpochTimestamp, barulah flux 2 akan sorting epoch timestamp tersebut dengan numerical order, karena itu merupakan time unix yang akan increase terus-menerus tiap detik nya

Untuk tag semantic version, flux 1 dan flux 2 cara kerjanya tetap sama, sort dengan nge lookup tag, jadi aman untuk semver di flux 1 maupun flux 2
```bash
# Epoch unix time
$ date +%s
1634263494

# Convert to readable format
$ date -d @1634263494
Fri Oct 15 09:04:54 AM WIB 2021
```

Lalu extensions flux yang dipasang disini ada 2,
- image-automation-controller  
Ini bakal ngescan tag-tag yang ada di image, ini teknik yang lebih manusiawi. Sebelumnya flux 1 bakal ngescan the whole registry, kalau pake ini kita bakal specific image mana yang mau di scan misalnya `asia.gcr.io/privysign/privysign/api-manage` lalu image itu tag-tag nya akan di scan, better than scan metadata image
- image-reflector-controller  
Lalu disini tugasnya memfilter tag yang sudah di scan oleh `image-automation-controller`, entah itu bisa di filter by semver, atau by tag-timestamp